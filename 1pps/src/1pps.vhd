library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pps_module is
    generic(
        ADDR_W : natural;
        DATA_W : natural
    );
    port(
        clk          : in  std_logic;
        rst_n        : in  std_logic;
        pps_pulse    : in  std_logic;
        timestamp    : out std_logic_vector(31 downto 0);
        -- Memory mapped register ports
        memory_cs    : in  std_logic;
        memory_rd    : in  std_logic;
        memory_addr  : in  std_logic_vector(ADDR_W - 1 downto 0);
        memory_rdata : out std_logic_vector(DATA_W - 1 downto 0)
    );
end entity pps_module;

architecture rtl of pps_module is

begin

end architecture rtl;
