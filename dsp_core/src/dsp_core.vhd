library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dsp_core is
    generic(
        C_CHANNEL_SIZE : natural;
        ADDR_W         : natural;
        DATA_W         : natural
    );
    port(
        clk          : in  std_logic;
        rst_n        : in  std_logic;
        -- Data in
        i_data_in    : in  std_logic_vector(C_CHANNEL_SIZE - 1 downto 0);
        i_valid_in   : in  std_logic;
        q_data_in    : in  std_logic_vector(C_CHANNEL_SIZE - 1 downto 0);
        q_valid_in   : in  std_logic;
        -- Data out
        i_data_out   : out std_logic_vector(C_CHANNEL_SIZE - 1 downto 0);
        i_valid_out  : out std_logic;
        q_data_out   : out std_logic_vector(C_CHANNEL_SIZE - 1 downto 0);
        q_valid_out  : out std_logic;
        -- Memory mapped register ports
        memory_cs    : in  std_logic;
        memory_rd    : in  std_logic;
        memory_addr  : in  std_logic_vector(ADDR_W - 1 downto 0);
        memory_rdata : out std_logic_vector(DATA_W - 1 downto 0)
    );
end entity dsp_core;

architecture rtl of dsp_core is

begin

    placeholder : process(clk) is
    begin
        if rising_edge(clk) then
            if rst_n = '0' then
                i_data_out  <= (others => '0');
                i_valid_out <= '0';
                q_data_out  <= (others => '0');
                q_valid_out <= '0';

            else
                i_data_out  <= i_data_in;
                i_valid_out <= i_valid_in;
                q_data_out  <= q_data_in;
                q_valid_out <= q_valid_in;
            end if;
        end if;
    end process placeholder;

end architecture rtl;
