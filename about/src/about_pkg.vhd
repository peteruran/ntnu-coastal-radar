--------------------------------------------------------------------------------
-- Company       : Rolls Royce / Zolve
-- Author        : Amund Fjosne
--                  _
--                 | |
--      ____  ___  | |__   __  ___
--     |_  / / _ \ | |\ \ / / /   \
--      / / | (_) || | \ V / |  __/
--     /___| \___/ |_|  \_/   \___|
--
-- Description:
-- This package contains a register list and other utilities used
-- in the about module
--
-- This package will be overwritten by a script during build
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package about_pkg is
    ---------------------------------------------------------------------------
    -- Register map
    ---------------------------------------------------------------------------
    constant C_ABT_REGMAP_GIT_HASH : natural := 0; -- R
    constant C_ABT_REGMAP_DDMMYY   : natural := 1; -- R
    constant C_ABT_REGMAP_HHMMSS   : natural := 2; -- R

    ---------------------------------------------------------------------------
    -- Automatically modified by build script
    ---------------------------------------------------------------------------
    constant C_GIT_HASH : unsigned(15 downto 0) := x"BABE";
    constant C_DDMMYY   : unsigned(15 downto 0) := x"CAFE";
    constant C_HHMMSS   : unsigned(15 downto 0) := x"FACE";

end package;
