--------------------------------------------------------------------------------
-- Company       : Rolls Royce / Zolve
-- Author        : Amund Fjosne
--                  _
--                 | |
--      ____  ___  | |__   __  ___
--     |_  / / _ \ | |\ \ / / /   \
--      / / | (_) || | \ V / |  __/
--     /___| \___/ |_|  \_/   \___|
--
-- Description:
-- This module contains read-only registers that contains information 
-- about the system. Eg. git hash, date of build, etc.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.about_pkg.all;

entity about_module is
    generic(
        ADDR_W    : natural;
        DATA_W : natural
    );
    port(
        clk          : in  std_logic;
        rst_n        : in  std_logic;
        -- Memory mapped register ports
        memory_cs    : in  std_logic;
        memory_rd    : in  std_logic;
        memory_addr  : in  std_logic_vector(ADDR_W - 1 downto 0);
        memory_rdata : out std_logic_vector(DATA_W - 1 downto 0)
    );
end entity;

architecture rtl of about_module is
begin

    --------------------------------------------------------------------------
    -- Read / Write Interface
    --------------------------------------------------------------------------

    cpu_rd_proc : process(clk)
        variable var_memory_rdata : std_logic_vector(DATA_W - 1 downto 0) := (others => '0');
    begin
        if rising_edge(clk) then

            if rst_n = '0' then
                memory_rdata <= (others => '0');
            else
                ---------------------------------------
                -- READ
                ---------------------------------------
                -- Always reset variable
                var_memory_rdata := (others => '0');

                if (memory_cs and memory_rd) = '1' then
                    case to_integer(unsigned(memory_addr)) is
                        when C_ABT_REGMAP_GIT_HASH =>
                            var_memory_rdata := std_logic_vector(resize(C_GIT_HASH, DATA_W));
                        when C_ABT_REGMAP_DDMMYY =>
                            var_memory_rdata := std_logic_vector(resize(C_DDMMYY, DATA_W));
                        when C_ABT_REGMAP_HHMMSS =>
                            var_memory_rdata := std_logic_vector(resize(C_HHMMSS, DATA_W));
                        when others =>  -- Do nothing
                    end case;

                    -- Set signal to variable value
                    memory_rdata <= var_memory_rdata;

                end if;
            end if;
        end if;
    end process;

end;
