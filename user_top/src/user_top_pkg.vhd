library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package user_top_pkg is

    ----------------------------------------------------------------------------
    -- Application specific config
    ---------------------------------------------------------------------------- 
    --Width of each ADC channel
    constant C_CHANNEL_SIZE : natural := 32;

    ----------------------------------------------------------------------------
    -- Data bus constants
    ----------------------------------------------------------------------------
    constant C_TOP_BRIDGE_ADDR_W : natural := 16;
    constant C_TOP_BRIDGE_DATA_W : natural := 16;
    constant C_TOP_MODULE_ADDR_W : natural := 8;
    constant C_TOP_MODULE_DATA_W : natural := 16;

    ----------------------------------------------------------------------------
    -- Module Address Index
    ----------------------------------------------------------------------------
    constant C_TOP_MODULE_ABOUT : natural := 0;
    constant C_TOP_MODULE_DEBUG : natural := 1;
    constant C_TOP_MODULE_1PPS  : natural := 2;
    constant C_TOP_MODULE_DSP   : natural := 3;

    ----------------------------------------------------------------------------
    -- Number of addressed modules in the design
    ----------------------------------------------------------------------------
    constant C_TOP_MODULE_COUNT : natural := 4;

    ----------------------------------------------------------------------------
    -- Types
    ----------------------------------------------------------------------------
    type T_STD_LOGIC_ARRAY is array (natural range <>) of std_logic_vector;

end package;
