library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.user_top_pkg.all;

entity user_top is
    port(
        clk         : in  std_logic;
        rst_n       : in  std_logic;
        -- Data in
        i_data_in   : in  std_logic_vector(C_CHANNEL_SIZE - 1 downto 0);
        i_valid_in  : in  std_logic;
        q_data_in   : in  std_logic_vector(C_CHANNEL_SIZE - 1 downto 0);
        q_valid_in  : in  std_logic;
        -- Data out
        i_data_out  : out std_logic_vector(C_CHANNEL_SIZE - 1 downto 0);
        i_valid_out : out std_logic;
        q_data_out  : out std_logic_vector(C_CHANNEL_SIZE - 1 downto 0);
        q_valid_out : out std_logic;
        -- UART
        uart_rx     : in  std_logic;
        uart_tx     : out std_logic;
        -- 1PPS
        pps_pulse   : in  std_logic
    );
end entity user_top;

architecture rtl of user_top is

    ----------------------------------------------------------------------------
    -- Signals used for memory mapped register interface
    ----------------------------------------------------------------------------

    -- Bridge (UART -> FPGA -> Addr decoder)
    signal bridge_rd          : std_logic;
    signal bridge_wr          : std_logic;
    signal bridge_addr        : std_logic_vector(C_TOP_BRIDGE_ADDR_W - 1 downto 0);
    signal bridge_rdata       : std_logic_vector(C_TOP_BRIDGE_DATA_W - 1 downto 0);
    signal bridge_raddr       : std_logic_vector(C_TOP_BRIDGE_ADDR_W - 1 downto 0);
    signal bridge_wdata       : std_logic_vector(C_TOP_BRIDGE_DATA_W - 1 downto 0);
    -- Module (Addr decoder -> modules/blocks)
    signal module_rd          : std_logic;
    signal module_wr          : std_logic;
    signal module_addr        : std_logic_vector(C_TOP_MODULE_ADDR_W - 1 downto 0);
    signal module_rdata_array : T_STD_LOGIC_ARRAY(0 to C_TOP_MODULE_COUNT - 1)(C_TOP_MODULE_DATA_W - 1 downto 0);
    signal module_wdata       : std_logic_vector(C_TOP_MODULE_DATA_W - 1 downto 0);
    signal module_cs_array    : std_logic_vector(C_TOP_MODULE_COUNT - 1 downto 0);

    ----------------------------------------------------------------------------
    -- DSP Core
    ----------------------------------------------------------------------------
    signal timestamp : std_logic_vector(31 downto 0);

begin

    addr_decoder_i : entity work.addr_decoder
        generic map(
            BRIDGE_ADDR_W => C_TOP_BRIDGE_ADDR_W,
            MODULE_ADDR_W => C_TOP_MODULE_ADDR_W,
            MODULE_DATA_W => C_TOP_MODULE_DATA_W,
            MODULE_COUNT  => C_TOP_MODULE_COUNT
        )
        port map(
            clk                => clk,
            rst_n              => rst_n,
            bridge_rd          => bridge_rd,
            bridge_wr          => bridge_wr,
            bridge_addr        => bridge_addr,
            bridge_wdata       => bridge_wdata,
            bridge_rdata       => bridge_rdata,
            bridge_raddr       => bridge_raddr,
            module_rdata_array => module_rdata_array,
            module_rd          => module_rd,
            module_wr          => module_wr,
            module_addr        => module_addr,
            module_wdata       => module_wdata,
            module_cs          => module_cs_array
        );

    about_module_i : entity work.about_module
        generic map(
            ADDR_W => C_TOP_MODULE_ADDR_W,
            DATA_W => C_TOP_MODULE_DATA_W
        )
        port map(
            clk          => clk,
            rst_n        => rst_n,
            memory_cs    => module_cs_array(C_TOP_MODULE_ABOUT),
            memory_rd    => module_rd,
            memory_addr  => module_addr,
            memory_rdata => module_rdata_array(C_TOP_MODULE_ABOUT)
        );

    dsp_core_i : entity work.dsp_core
        generic map(
            ADDR_W         => C_TOP_MODULE_ADDR_W,
            DATA_W         => C_TOP_MODULE_DATA_W,
            C_CHANNEL_SIZE => C_CHANNEL_SIZE
        )
        port map(
            clk          => clk,
            rst_n        => rst_n,
            i_data_in    => i_data_in,
            i_valid_in   => i_valid_in,
            q_data_in    => q_data_in,
            q_valid_in   => q_valid_in,
            i_data_out   => i_data_out,
            i_valid_out  => i_valid_out,
            q_data_out   => q_data_out,
            q_valid_out  => q_valid_out,
            memory_cs    => module_cs_array(C_TOP_MODULE_DSP),
            memory_rd    => module_rd,
            memory_addr  => module_addr,
            memory_rdata => module_rdata_array(C_TOP_MODULE_DSP)
        );

    pps_module_inst : entity work.pps_module
        generic map(
            ADDR_W => C_TOP_MODULE_ADDR_W,
            DATA_W => C_TOP_MODULE_DATA_W
        )
        port map(
            clk          => clk,
            rst_n        => rst_n,
            pps_pulse    => pps_pulse,
            timestamp    => timestamp,
            memory_cs    => module_cs_array(C_TOP_MODULE_1PPS),
            memory_rd    => module_rd,
            memory_addr  => module_addr,
            memory_rdata => module_rdata_array(C_TOP_MODULE_1PPS)
        );

end architecture rtl;
