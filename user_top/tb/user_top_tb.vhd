library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;

library vunit_lib;
context vunit_lib.vunit_context;

use work.user_top_pkg.all;

entity user_top_tb is
    generic(
        runner_cfg  : string;
        output_path : string;
        tb_path     : string
    );

end entity user_top_tb;

architecture sim of user_top_tb is
    signal clk         : std_logic                                     := '0';
    signal rst_n       : std_logic                                     := '0';
    signal i_data_in   : std_logic_vector(C_CHANNEL_SIZE - 1 downto 0) := (others => '0');
    signal i_valid_in  : std_logic                                     := '0';
    signal q_data_in   : std_logic_vector(C_CHANNEL_SIZE - 1 downto 0) := (others => '0');
    signal q_valid_in  : std_logic                                     := '0';
    signal i_data_out  : std_logic_vector(C_CHANNEL_SIZE - 1 downto 0);
    signal i_valid_out : std_logic;
    signal q_data_out  : std_logic_vector(C_CHANNEL_SIZE - 1 downto 0);
    signal q_valid_out : std_logic;
    signal uart_rx     : std_logic                                     := '1';
    signal uart_tx     : std_logic;
    signal pps_pulse   : std_logic                                     := '0';

    constant C_CLK_PERIOD : time := 10 ns;
begin

    clk <= not clk after (C_CLK_PERIOD / 2);

    --    pps_gen : process
    --    begin
    --        wait for 1000 ms - C_CLK_PERIOD;
    --        pps_pulse <= '1';
    --        wait for C_CLK_PERIOD;
    --        pps_pulse <= '0';
    --    end process;

    --------------------------------------------------------------------------------
    -- DUT initialization
    --------------------------------------------------------------------------------
    dut : entity work.user_top
        port map(
            clk         => clk,
            rst_n       => rst_n,
            i_data_in   => i_data_in,
            i_valid_in  => i_valid_in,
            q_data_in   => q_data_in,
            q_valid_in  => q_valid_in,
            i_data_out  => i_data_out,
            i_valid_out => i_valid_out,
            q_data_out  => q_data_out,
            q_valid_out => q_valid_out,
            uart_rx     => uart_rx,
            uart_tx     => uart_tx,
            pps_pulse   => pps_pulse
        );

    --------------------------------------------------------------------------------
    -- Main testbench process
    --------------------------------------------------------------------------------
    main : process
        --------------------------------------------------------------------------------
        -- Procedure to write IQ data to file for post-check
        --------------------------------------------------------------------------------
        file fwrite : text;
        variable l  : line;

        procedure write_iq_data(i_data : i_data_out'subtype;
                                q_data : i_data_out'subtype
                               ) is
        begin
            write(l, to_hstring(i_data) & " " & to_hstring(q_data));
            writeline(fwrite, l);

        end procedure write_iq_data;

        --------------------------------------------------------------------------------
        -- Procedure to read IQ data from pre-check
        --------------------------------------------------------------------------------
        file text_file     : text open read_mode is "/home/peter/bin/radar_user_logic/user_top/radar.dat";
        variable text_line : line;
        variable ok        : boolean;
        variable data      : i_data_in'subtype;

    begin
        test_runner_setup(runner, runner_cfg);
        report "Testbench started.";
        report "Hello world!";

        file_open(fwrite, output_path & "results.txt", write_mode);

        -- Take out of reset
        wait for 10 * C_CLK_PERIOD;
        report "Taking DUT out of reset";
        rst_n <= '1';

        report "Loop through stimulus file";
        while not endfile(text_file) loop

            readline(text_file, text_line);

            -- Skip empty lines and single-line comments
            if text_line.all'length = 0 or text_line.all(1) = '#' then
                next;
            end if;

            -- Read I-sample
            hread(text_line, data, ok);
            assert ok
            report "Read 'i-sample' failed for line: " & text_line.all
            severity failure;
            i_data_in <= data;
            report "Read i-channel stimulus: " & to_string(data);

            -- Read Q-sample
            hread(text_line, data, ok);
            assert ok
            report "Read 'q-sample' failed for line: " & text_line.all
            severity failure;
            q_data_in <= data;
            report "Read q-channel stimulus: " & to_string(data);

            -- Assert and deassert valid signals
            i_valid_in <= '1';
            q_valid_in <= '1';
            wait for C_CLK_PERIOD;
            i_valid_in <= '0';
            q_valid_in <= '0';

            write_iq_data(i_data_out, q_data_out);

        end loop;

        -- Quick fix
        write_iq_data(x"00000000", x"00000000");
        report "Finished reading stimulus file.";

        report "Wrote data to file: " & output_path & "results.txt" severity note;

        -- End simulation
        file_close(fwrite);
        test_runner_cleanup(runner);
    end process;

end architecture sim;
