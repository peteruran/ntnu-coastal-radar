"""
Module to generate radar data for VHDL simulation.
"""
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt

import logging
logger = logging.getLogger(__name__)

class Radar:
    """Class to generate and verify radar waveforms."""
    def __init__(self):
        self.iq_data = None
        self.t = None

    def generate_iq_data(self, freq, prf, step_size=10*1e-9, duration = 100*1e-6):
        """Generate radar waveform.

        Generates complex radar waveform for given freq.
        Emulates a coherent transmitter by multiplying the waveform
        with a square wave with a freuqency of PRF and duty cycle 50 %.

        Default duration is 100 us with a step size of 10 ns.

        Returns:
            iq_data (np.complex128): Radar waveform
        """

        num_of_samples = np.int(np.round(duration/step_size))
        logger.info("Number of samples to generate: " + str(num_of_samples))

        # Generate IQ data
        self.t = np.linspace(0, duration, num_of_samples)
        i_data = (np.cos(2*np.pi*freq*self.t) + 1) / 2
        q_data = (np.sin(2*np.pi*freq*self.t) + 1) / 2
        self.iq_data = i_data + 1j*q_data

        # Create coherent pulses for given PRF
        cpi = (signal.square(2*np.pi*prf*self.t, duty=.1) + 1) / 2
        self.iq_data *= cpi

        return (self.t, self.iq_data)

    def plot_iq_data(self, iq_data):
        """Plot IQ-data. Convenience function."""
        plt.plot(iq_data.real, label='Real part')
        plt.plot(iq_data.imag, label='Imaginary part')
        plt.show()

    def save_iq_data(self, iq_data, channel_width=16, file='radar.dat'):
        """Save IQ-data to textfile hex.

        Data is scaled to 2**channel_width-1 and saved as uint32.
        Data is saved as zero-padded eight digit hexadecimal numbers.

        Data is saved in pairs of I and Q using space as a delimiter,
        as shown below.

        iiiiiiii qqqqqqqq
        iiiiiiii qqqqqqqq
        iiiiiiii qqqqqqqq
        ...

        Returns:
            Nothing
        """
        iq_data_scaled = iq_data * (2**channel_width - 1) # scale to channel width
        data = np.column_stack(
            (iq_data_scaled.real.astype(dtype=np.uint32),
            iq_data_scaled.imag.astype(dtype=np.uint32))
            )
        np.savetxt(file, data, delimiter=" ", fmt="%08x")

    def load_iq_data(self, channel_width=16, file='radar.dat'):
        """Load IQ-data from textfile.

        See save_iq_data docstring for format.

        """
        data = np.loadtxt(file, dtype=np.uint32, delimiter=' ',
            converters={_: lambda s: int(s, 16) for _ in range(2)})
        iq_data = data[:,0] + 1j*data[:,1]
        iq_data /= (2**channel_width - 1) # normalize
        return iq_data

    def pre_config(self, output_path):
        """VUnit pre_config function."""
        t, iq_data = self.generate_iq_data(freq=5e6, prf=.1e6)
        self.save_iq_data(iq_data, file=output_path + '/radar.dat')
        return True

    def post_check(self, output_path):
        """VUnit post_check function."""
        iq_data_sim = self.load_iq_data(file=output_path + '/results.txt')
        self.compare_plots(iq_data_sim)
        return True

    def compare_plots(self, sim_iq_data):
        """Plots time and frequency domain plots for pre- and post-sim data."""

        plt.subplot(2,2,1)
        plt.title('Pre-sim time domain')
        plt.plot(self.t*1e6, self.iq_data.real, 'o.', label='I-channel')
        plt.plot(self.t*1e6, self.iq_data.imag, 'b.', label='Q-channel')
        plt.xlabel('Time [us]')

        plt.subplot(2,2,2)
        plt.title('Pre-sim power spectrum')
        power_spectrum = np.abs(np.fft.fft(self.iq_data))**2
        freq = np.fft.fftfreq(self.t.shape[-1], self.t[1]-self.t[0])
        plt.plot(freq, power_spectrum, label='Power spectrum')
        plt.xlabel('Frequency [Hz]')

        plt.subplot(2,2,3)
        plt.title('Post-sim data')
        plt.plot(sim_iq_data.real, label='I-channel', marker='.')
        plt.plot(sim_iq_data.imag, label='Q-channel', marker='.')
        plt.xlabel('Time [us]')

        plt.subplot(2,2,4)
        plt.title('Post-sim power spectrum')
        power_spectrum = np.abs(np.fft.fft(sim_iq_data))**2
        plt.plot(freq, power_spectrum, label='Power spectrum')
        plt.xlabel('Frequency [Hz]')

        plt.legend()
        plt.grid()
        plt.show()



if __name__ == '__main__':
    """Short test to verify generation, saving and loading to file."""

    logger.info("Start testbench.")
    # Generate radar data
    radar = Radar()
    logger.info("Generate radar data.")
    t, iq_data = radar.generate_iq_data(freq=5e6, prf=.1e6)

    # Save and load IQ data to and from file
    logger.info("Save IQ data to file.")
    radar.save_iq_data(iq_data)
    logger.info("Load IQ data from file")
    iq_data_loaded = radar.load_iq_data()

    logger.info("Plot values")
    plt.subplot(2,1,1)
    plt.plot(t, iq_data.real, label='Real part', marker='.')
    plt.plot(t, iq_data.imag, label='Imaginary part', marker='.')
    plt.xlabel('Seconds')

    plt.subplot(2,1,2)
    plt.plot(t, iq_data_loaded.real, label='Real part', marker='.')
    plt.plot(t, iq_data_loaded.imag, label='Imaginary part', marker='.')
    plt.show()
