from vunit import VUnit

# Create radar object for data generation and validation
from radar import Radar
radar = Radar()

# Create VUnit instance by parsing command line arguments
vu = VUnit.from_argv()

# Create library 'lib'
lib = vu.add_library("lib")

# Add testbench and all source files in project
lib.add_source_files("../*/src/*.vhd")
lib.add_source_file("./tb/user_top_tb.vhd")

# Add pre_config and post_check to testbench
user_top_tb = lib.test_bench("user_top_tb")
user_top_tb.set_pre_config(radar.pre_config)
user_top_tb.set_post_check(radar.post_check)

# Run vunit function
vu.main()
