library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

use work.user_top_pkg.all;

entity comms_tb is
    generic(runner_cfg : string);
end entity comms_tb;

architecture sim of comms_tb is
    signal clk                   : std_logic := '0';
    signal rst_n                 : std_logic := '0';
    signal uart_tx               : std_logic;
    signal uart_rx               : std_logic;
    signal bridge_waddr          : std_logic_vector(C_TOP_BRIDGE_ADDR_W - 1 downto 0);
    signal bridge_wdata          : std_logic_vector(C_TOP_MODULE_DATA_W - 1 downto 0);
    signal bridge_rdata          : std_logic_vector(C_TOP_MODULE_DATA_W - 1 downto 0);
    signal bridge_addr           : std_logic_vector(C_TOP_BRIDGE_ADDR_W - 1 downto 0);
    signal bridge_read_not_write : std_logic;
    signal valid_in              : std_logic;
    signal valid_out             : std_logic;

begin

    dut : entity work.comms
        generic map(
            BRIDGE_ADDR_W => C_TOP_BRIDGE_ADDR_W,
            MODULE_ADDR_W => C_TOP_MODULE_ADDR_W,
            MODULE_DATA_W => C_TOP_MODULE_DATA_W
        )
        port map(
            clk                   => clk,
            rst_n                 => rst_n,
            uart_tx               => uart_tx,
            uart_rx               => uart_rx,
            bridge_waddr          => bridge_waddr,
            bridge_wdata          => bridge_wdata,
            bridge_rdata          => bridge_rdata,
            bridge_addr           => bridge_addr,
            bridge_read_not_write => bridge_read_not_write,
            valid_in              => valid_in,
            valid_out             => valid_out
        );

    check : process
    begin
        test_runner_setup(runner, runner_cfg);

        report "Hello world";

        test_runner_cleanup(runner);    -- Simulation ends here
    end process;

end architecture sim;
