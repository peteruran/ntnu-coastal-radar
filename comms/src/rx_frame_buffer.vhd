library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rx_frame_buffer is
    generic(
        C_FRAME_SIZE : natural
    );
    port(
        clk        : in  std_logic;
        rst_n      : in  std_logic;
        byte       : in  std_logic_vector(7 downto 0);
        valid_in   : in  std_logic;
        uart_error : in  std_logic;
        frame      : out std_logic_vector(39 downto 0);
        valid_out  : out std_logic
    );
end entity rx_frame_buffer;

architecture rtl of rx_frame_buffer is
    signal byte_counter : natural;

begin

    frame_buffer_proc : process(clk) is
    begin
        if rising_edge(clk) then
            if rst_n = '0' then
                byte_counter <= 0;
                frame        <= (others => '0');
                valid_out    <= '0';
            else
                if uart_error = '1' then
                    byte_counter <= 0;
                    frame        <= (others => '0');
                    valid_out    <= '0';
                elsif valid_in = '1' then
                    if byte_counter = C_FRAME_SIZE / 8 - 1 then
                        valid_out    <= '1';
                        byte_counter <= 0;
                    else
                        frame((byte_counter + 1) * 8 - 1 downto byte_counter * 8) <= byte;
                        byte_counter                                              <= byte_counter + 1;
                        valid_out                                                 <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process frame_buffer_proc;

end architecture rtl;
