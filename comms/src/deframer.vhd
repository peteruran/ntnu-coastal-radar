library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity deframer is
    generic(
        C_FRAME_SIZE  : natural;
        BRIDGE_ADDR_W : natural;
        MODULE_DATA_W : natural
    );
    port(
        clk                   : in  std_logic;
        rst_n                 : in  std_logic;
        frame                 : in  std_logic_vector(C_FRAME_SIZE - 1 downto 0);
        valid_in              : in  std_logic;
        bridge_addr           : out std_logic_vector(BRIDGE_ADDR_W - 1 downto 0);
        bridge_wdata          : out std_logic_vector(MODULE_DATA_W - 1 downto 0);
        bridge_read_not_write : out std_logic
    );
end entity deframer;

architecture rtl of deframer is
begin

    deframer_proc : process(clk) is
        variable frame_type : natural := 0;

    begin
        if rising_edge(clk) then
            if rst_n = '0' then
            else
                if valid_in = '1' then
                    frame_type := to_integer(unsigned(frame(frame'high - 1 downto frame'high - 4)));

                    case frame_type is
                        when 0 =>
                            bridge_wdata          <= frame(MODULE_DATA_W - 1 downto 0);
                            bridge_addr           <= frame(BRIDGE_ADDR_W + MODULE_DATA_W - 1 downto MODULE_DATA_W - 1);
                            bridge_read_not_write <= frame(frame'high);
                        when others => null;
                    end case;

                end if;
            end if;
        end if;
    end process deframer_proc;

end architecture rtl;
