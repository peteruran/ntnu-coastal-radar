library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tx_frame_buffer is
    port(
        clk       : in  std_logic;
        rst_n     : in  std_logic;
        frame     : in  std_logic_vector(39 downto 0);
        valid_in  : in  std_logic;
        ready_out : in  std_logic;
        valid_out : out std_logic;
        byte      : out std_logic_vector(7 downto 0)
    );
end entity tx_frame_buffer;

architecture rtl of tx_frame_buffer is
    signal byte_counter : natural;
    signal frame_sig    : frame'subtype;
    signal enable       : std_logic;
begin

    tx_buffer_proc : process(clk) is
    begin
        if rising_edge(clk) then
            if rst_n = '0' then
                byte         <= (others => '0');
                valid_out    <= '0';
                byte_counter <= 0;
                frame_sig    <= (others => '0');
                enable       <= '0';
            else
                valid_out <= '0';
                if valid_in = '1' then
                    frame_sig    <= frame;
                    byte_counter <= 0;
                    enable       <= '1';
                elsif enable = '1' then
                    --valid_out <= '0';
                    if ready_out = '1' then
                        byte         <= frame(8 * byte_counter to 8 * (byte_counter + 1) - 1);
                        valid_out    <= '1';
                        byte_counter <= byte_counter + 1;
                    end if;
                end if;
            end if;
        end if;
    end process tx_buffer_proc;

end architecture rtl;
