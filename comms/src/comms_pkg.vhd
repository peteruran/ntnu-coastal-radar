library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package comms_pkg is

    ----------------------------------------------------------------------------
    -- Clock and baud rates
    ---------------------------------------------------------------------------- 
    constant C_CLOCK_FREQUENCY : real    := 100.0e6;
    constant C_BAUD_RATE       : natural := 115200;

    ----------------------------------------------------------------------------
    -- Frame specifications
    ---------------------------------------------------------------------------- 
    constant C_FRAME_SIZE : natural := 40;

end package;
