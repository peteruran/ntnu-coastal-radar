library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity framer is
    generic(
        C_FRAME_SIZE  : natural;
        BRIDGE_ADDR_W : natural;
        MODULE_DATA_W : natural
    );
    port(
        clk          : in  std_logic;
        rst_n        : in  std_logic;
        valid_in     : in  std_logic;
        bridge_rdata : in  std_logic_vector(MODULE_DATA_W - 1 downto 0);
        bridge_raddr : in  std_logic_vector(BRIDGE_ADDR_W - 1 downto 0);
        frame        : out std_logic_vector(39 downto 0);
        valid_out    : out std_logic
    );
end entity framer;

architecture rtl of framer is
    signal header  : std_logic_vector(7 downto 0);
    signal payload : std_logic_vector(7 downto 0);
begin

    framer_proc : process(clk) is
    begin
        if rising_edge(clk) then
            if rst_n = '0' then
                frame   <= (others => '0');
                header  <= (others => '0');
                payload <= (others => '0');
            else

                if valid_in = '1' then
                    valid_out <= '1';

                    -- Frame type 0
                    frame(MODULE_DATA_W - 1 downto 0)                                 <= bridge_rdata;
                    frame(BRIDGE_ADDR_W + MODULE_DATA_W - 1 downto MODULE_DATA_W - 1) <= bridge_raddr;
                else
                    valid_out <= '0';
                end if;
            end if;
        end if;
    end process framer_proc;

end architecture rtl;
