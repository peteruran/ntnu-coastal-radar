library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.comms_pkg.all;

entity comms is
    generic(
        BRIDGE_ADDR_W : natural;
        MODULE_ADDR_W : natural;
        MODULE_DATA_W : natural
    );
    port(
        clk                   : in  std_logic;
        rst_n                 : in  std_logic;
        uart_tx               : out std_logic;
        uart_rx               : in  std_logic;
        bridge_waddr          : in  std_logic_vector(BRIDGE_ADDR_W - 1 downto 0);
        bridge_wdata          : out std_logic_vector(MODULE_DATA_W - 1 downto 0);
        bridge_rdata          : in  std_logic_vector(MODULE_DATA_W - 1 downto 0);
        bridge_addr           : out std_logic_vector(BRIDGE_ADDR_W - 1 downto 0);
        bridge_read_not_write : out std_logic;
        valid_in              : in  std_logic;
        valid_out             : out std_logic
    );
end entity comms;

architecture rtl of comms is
    ----------------------------------------------------------------------------
    -- UART
    ----------------------------------------------------------------------------
    signal uart_rx_data   : std_logic_vector(7 downto 0);
    signal uart_rx_valid  : std_logic;
    signal stop_bit_error : std_logic;
    signal uart_tx_valid  : std_logic;
    signal uart_tx_data   : std_logic_vector(7 downto 0);
    signal busy           : std_logic;
    signal tx_byte        : std_logic_vector(7 downto 0);
    signal buffer_valid   : std_logic;
    signal tx_frame       : std_logic_vector(39 downto 0);
    signal tx_uart_busy   : std_logic;
    signal rx_frame       : std_logic_vector(39 downto 0);
    signal rx_byte        : std_logic_vector(7 downto 0);
    signal rx_frame_valid : std_logic;
    signal tx_frame_valid : std_logic;
begin

    uart_rx_i : entity work.uart_rx
        generic map(
            clock_frequency => C_CLOCK_FREQUENCY,
            baud_rate       => C_BAUD_RATE
        )
        port map(
            clk            => clk,
            rst_n          => rst_n,
            rx             => uart_rx,
            data           => rx_byte,
            valid          => uart_rx_valid,
            stop_bit_error => stop_bit_error
        );

    uart_tx_i : entity work.uart_tx
        generic map(
            clock_frequency => C_CLOCK_FREQUENCY,
            baud_rate       => C_BAUD_RATE
        )
        port map(
            clk   => clk,
            rst_n => rst_n,
            start => buffer_valid,
            data  => tx_byte,
            busy  => tx_uart_busy,
            tx    => uart_tx
        );

    framer_i : entity work.framer
        generic map(
            C_FRAME_SIZE  => C_FRAME_SIZE,
            BRIDGE_ADDR_W => BRIDGE_ADDR_W,
            MODULE_DATA_W => MODULE_DATA_W
        )
        port map(
            clk          => clk,
            rst_n        => rst_n,
            valid_in     => valid_in,
            bridge_rdata => bridge_rdata,
            bridge_raddr => bridge_waddr,
            frame        => tx_frame,
            valid_out    => tx_frame_valid
        );

    frame_buffer_i : entity work.tx_frame_buffer
        port map(
            clk       => clk,
            rst_n     => rst_n,
            frame     => tx_frame,
            valid_in  => tx_frame_valid,
            ready_out => not tx_uart_busy,
            valid_out => buffer_valid,
            byte      => tx_byte
        );

    rx_frame_buffer_i : entity work.rx_frame_buffer
        generic map(
            C_FRAME_SIZE => C_FRAME_SIZE
        )
        port map(
            clk        => clk,
            rst_n      => rst_n,
            byte       => rx_byte,
            valid_in   => uart_rx_valid,
            uart_error => stop_bit_error,
            frame      => rx_frame,
            valid_out  => rx_frame_valid
        );

    deframer_inst : entity work.deframer
        generic map(
            C_FRAME_SIZE  => C_FRAME_SIZE,
            BRIDGE_ADDR_W => BRIDGE_ADDR_W,
            MODULE_DATA_W => MODULE_DATA_W
        )
        port map(
            clk                   => clk,
            rst_n                 => rst_n,
            frame                 => rx_frame,
            valid_in              => rx_frame_valid,
            bridge_addr           => bridge_addr,
            bridge_wdata          => bridge_wdata,
            bridge_read_not_write => bridge_read_not_write
        );

end architecture rtl;
