from vunit import VUnit

# Create VUnit instance by parsing command line arguments
vu = VUnit.from_argv()

# Create library 'lib'
lib = vu.add_library("lib")

# Add all files ending in .vhd in current working directory to library
lib.add_source_file("../user_top/src/user_top_pkg.vhd")

lib.add_source_files("../uart/src/*.vhd")

lib.add_source_files("./*/*.vhd")

# Run vunit function
vu.main()
