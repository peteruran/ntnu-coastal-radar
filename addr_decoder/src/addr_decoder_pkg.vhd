--------------------------------------------------------------------------------
-- Company       : Rolls Royce / Zolve
-- Author        : Pter Uran
--                  _
--                 | |
--      ____  ___  | |__   __  ___
--     |_  / / _ \ | |\ \ / / /   \
--      / / | (_) || | \ V / |  __/
--     /___| \___/ |_|  \_/   \___|
--
-- Description:
-- Address decoder module package.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package addr_decoder_pkg is

    ----------------------------------------------------------------------------
    -- Set interface with access to memory-mapped registers
    -- '0' for host link, '1' for interdad
    ----------------------------------------------------------------------------
    constant C_ADDR_DECODER_IFACE_SELECT : std_logic := '1';

end package;
