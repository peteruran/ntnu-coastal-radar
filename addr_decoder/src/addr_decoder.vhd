--------------------------------------------------------------------------------
-- Company       : Zolve
-- Author        : Erlend Troite, Peter Uran
--                  _
--                 | |
--      ____  ___  | |__   __  ___
--     |_  / / _ \ | |\ \ / / /   \
--      / / | (_) || | \ V / |  __/
--     /___| \___/ |_|  \_/   \___|
--
-- Description:
-- Module for decoding addresses for addressed data bus into chip selects. 
-- Input: Address
-- Output: Chip select vector for addressing the different modules + the remaining address
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Libraries
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.user_top_pkg.all;

entity addr_decoder is
    generic(
        BRIDGE_ADDR_W : natural;
        MODULE_ADDR_W : natural;
        MODULE_DATA_W : natural;
        MODULE_COUNT  : natural
    );
    port(
        clk                : in  std_logic;
        rst_n              : in  std_logic;
        -- Bridge
        bridge_rd          : in  std_logic;
        bridge_wr          : in  std_logic;
        bridge_addr        : in  std_logic_vector(BRIDGE_ADDR_W - 1 downto 0);
        bridge_wdata       : in  std_logic_vector(MODULE_DATA_W - 1 downto 0);
        bridge_rdata       : out std_logic_vector(MODULE_DATA_W - 1 downto 0);
        bridge_raddr       : out std_logic_vector(BRIDGE_ADDR_W - 1 downto 0);
        -- Module
        module_rdata_array : in  T_STD_LOGIC_ARRAY(0 to MODULE_COUNT - 1)(MODULE_DATA_W - 1 downto 0);
        module_rd          : out std_logic;
        module_wr          : out std_logic;
        module_addr        : out std_logic_vector(MODULE_ADDR_W - 1 downto 0);
        module_wdata       : out std_logic_vector(MODULE_DATA_W - 1 downto 0);
        module_cs          : out std_logic_vector(MODULE_COUNT - 1 downto 0)
    );
end entity;

architecture behavioral of addr_decoder is
    signal r_index : natural range 0 to MODULE_COUNT - 1;

begin

    --------------------------------------------------------------------------
    -- 
    --------------------------------------------------------------------------
    interface_proc : process(clk)
        variable v_index_request : natural;
        variable v_index_valid   : boolean;
        variable v_index         : natural range 0 to MODULE_COUNT - 1;
        variable v_cs            : std_logic_vector(MODULE_COUNT - 1 downto 0);

    begin
        if rising_edge(clk) then

            if (rst_n = '0') then

                module_cs    <= (others => '0');
                module_rd    <= '0';
                module_wr    <= '0';
                module_addr  <= (others => '0');
                module_wdata <= (others => '0');

                r_index <= 0;

            else

                --------------------------------------------------------------------------
                -- Reset / Set variables
                --------------------------------------------------------------------------
                v_index_request := to_integer(unsigned(bridge_addr(BRIDGE_ADDR_W - 1 downto MODULE_ADDR_W)));
                v_index         := 0;
                v_index_valid   := FALSE;
                v_cs            := (others => '0');

                --------------------------------------------------------------------------
                -- Find index. Verify validity
                --------------------------------------------------------------------------    
                if v_index_request < MODULE_COUNT then
                    v_index       := v_index_request;
                    v_index_valid := TRUE;
                end if;

                --------------------------------------------------------------------------
                -- If index is valid, set chip select bit.
                --------------------------------------------------------------------------
                for i in 0 to MODULE_COUNT - 1 loop
                    if i = v_index and v_index_valid then
                        v_cs(i) := '1';
                    else
                        v_cs(i) := '0';
                    end if;
                end loop;

                --------------------------------------------------------------------------
                -- Passthrough
                --------------------------------------------------------------------------
                module_cs    <= v_cs;
                module_rd    <= bridge_rd;
                module_wr    <= bridge_wr;
                module_addr  <= bridge_addr(MODULE_ADDR_W - 1 downto 0);
                module_wdata <= bridge_wdata;

                --------------------------------------------------------------------------
                -- Register index
                --------------------------------------------------------------------------
                if (bridge_rd or bridge_wr) = '1' then
                    r_index <= v_index;
                end if;

                --------------------------------------------------------------------------
                -- rdata mux
                -- Pass requested address along with data to avoid ambiguity
                --------------------------------------------------------------------------
                bridge_rdata <= module_rdata_array(r_index);
                bridge_raddr <= bridge_addr;

            end if;

        end if;
    end process;
end architecture;

